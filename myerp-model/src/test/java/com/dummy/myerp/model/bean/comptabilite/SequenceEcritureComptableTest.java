package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SequenceEcritureComptableTest {

    List<JournalComptable> journalComptableList= new ArrayList<JournalComptable>();

    List<SequenceEcritureComptable> sequenceEcritureComptableList = new ArrayList<SequenceEcritureComptable>();


    @Mock
    JournalComptable journalComptable;

    @InjectMocks
    SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable();

    /**
     * Verifier code par année  - n'existe pas pour 2021 dans la base
     */
    @Test
    public void getByCodeAndYearTest(){

        when(journalComptable.getCode()).thenReturn("AC");

        sequenceEcritureComptable.setJournalCode( journalComptable.getCode() );
        sequenceEcritureComptable.setAnnee(2021);
        sequenceEcritureComptable.setDerniereValeur(1);

        sequenceEcritureComptableList.clear();
        sequenceEcritureComptableList.add( sequenceEcritureComptable );

        Assert.assertNotNull(SequenceEcritureComptable.getByJournalCodeAndYear(sequenceEcritureComptableList,"AC",2021) );
        Assert.assertEquals(sequenceEcritureComptable.getDerniereValeur(), SequenceEcritureComptable.getByJournalCodeAndYear(sequenceEcritureComptableList,"AC",2021).getDerniereValeur());
        Assert.assertNull(SequenceEcritureComptable.getByJournalCodeAndYear(sequenceEcritureComptableList,"AC",2000) );

    }

    /**
     * Verifier si SequenceEcritureComptable existe pour "AC" "2021" (true) faux pour les autres années
     *
     */
    @Test
    public void isSequenceEcritureComptableExistTest(){

        when(journalComptable.getCode()).thenReturn("AC");

        sequenceEcritureComptable.setJournalCode( journalComptable.getCode() );
        sequenceEcritureComptable.setAnnee(2021);
        sequenceEcritureComptable.setDerniereValeur(1);

        Assert.assertTrue(SequenceEcritureComptable.isSequenceEcritureComptableExiste(sequenceEcritureComptable,"AC",2021) );
        Assert.assertFalse(SequenceEcritureComptable.isSequenceEcritureComptableExiste(sequenceEcritureComptable,"BQ",2013) );
        Assert.assertFalse(SequenceEcritureComptable.isSequenceEcritureComptableExiste(sequenceEcritureComptable,"AC",1998) );
        Assert.assertFalse(SequenceEcritureComptable.isSequenceEcritureComptableExiste(null,"AC",1984) );
    }

}
