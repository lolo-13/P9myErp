package com.dummy.myerp.model.bean.comptabilite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
public class CompteComptableTest {

    @InjectMocks
    CompteComptable compteComptable401 = new CompteComptable(401, "Fournisseurs");

    @InjectMocks
    CompteComptable compteComptable411 = new CompteComptable(411, "Banque");

    /**
     * test de isLibelleCompteComptableCorrespondCodeCompteComptable
     */
    @Test
    public void isLibelleCompteComptableCorrespondCodeCompteComptableTest() {

        List<CompteComptable> compteComptables = new ArrayList<>();
        compteComptables.add(compteComptable411);
        compteComptables.add(compteComptable401);

        assertEquals( CompteComptable.getByNumero(compteComptables, 401).getLibelle(), "Fournisseurs");
        assertNotEquals( CompteComptable.getByNumero(compteComptables, 411).getLibelle(),"Lbp");
        assertNull( CompteComptable.getByNumero(compteComptables, 412));
    }

    /**
     * test de isCompteComptableExist
     */
    @Test
    public void isCompteComptableExistTest(){

        assertEquals(401, (int) compteComptable401.getNumero());
        assertNotEquals(411, (int) compteComptable401.getNumero());

    }
}
