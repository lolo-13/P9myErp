package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LigneEcritureComptableTest {
    @Mock
    EcritureComptable ecritureComptable;


    List<LigneEcritureComptable> ligneEcritureComptableList = new ArrayList<>();

    @InjectMocks
    LigneEcritureComptable ligneEcritureComptable ;


    @InjectMocks
    CompteComptable compteComptable411 = new CompteComptable(411, "Clients");

    @InjectMocks
    CompteComptable compteComptable706 = new CompteComptable(700, "Prestations de services");

    final int ligneId411 = 1;
    final int ecritureComptableId = -4;

    @Test
    public void getListeEcritureByEcritureComptableIdAndLigneId(){

        when(ecritureComptable.getId()).thenReturn(ecritureComptableId);

        ligneEcritureComptableList.clear();

        ligneEcritureComptable.setLigneId(ligneId411);
        ligneEcritureComptable.setCompteComptable( compteComptable411);
        ligneEcritureComptable.setLibelle( "Facture C110004" );
        ligneEcritureComptable.setDebit( new BigDecimal("5700.00") );
        ligneEcritureComptable.setCredit(null);
        ligneEcritureComptable.setEcritureComptableId( ecritureComptable.getId());

        ligneEcritureComptableList.add( ligneEcritureComptable );

        ligneEcritureComptable.setLigneId(2);
        ligneEcritureComptable.setCompteComptable( compteComptable706);
        ligneEcritureComptable.setLibelle( "TMA Appli Xxx" );
        ligneEcritureComptable.setDebit( null );
        ligneEcritureComptable.setCredit(new BigDecimal("4750.00"));
        ligneEcritureComptable.setEcritureComptableId( ecritureComptable.getId());

        ligneEcritureComptableList.add( ligneEcritureComptable );

        //au moins une ligneEcritureComptable pour cette écriture
        assertNotNull( LigneEcritureComptable.getLigneByEcritureComptableId(ligneEcritureComptableList,ecritureComptableId) );

    }

    /**
     * Verifier si la ligne comptable qui correspond à  id == "ligneId411" existe
     */
    @Test
    public void isLigneEcritureComptableExistTest(){

        when(ecritureComptable.getId()).thenReturn(ecritureComptableId);

        ligneEcritureComptable.setLigneId(ligneId411);
        ligneEcritureComptable.setCompteComptable( compteComptable411);
        ligneEcritureComptable.setLibelle( "Facture C110004" );
        ligneEcritureComptable.setDebit( new BigDecimal("5700.00") );
        ligneEcritureComptable.setCredit(null);
        ligneEcritureComptable.setEcritureComptableId( ecritureComptable.getId());

        ligneEcritureComptableList.clear();
        ligneEcritureComptableList.add( ligneEcritureComptable );

        //exactement cette ligneEcritureComptable selon idLigne et idEcritureComptable
        assertEquals(Optional.of(ligneId411), Optional.ofNullable(LigneEcritureComptable.getLigneByIds(ligneEcritureComptableList, ligneId411, ecritureComptableId).getLigneId()));

    }
}
