package com.dummy.myerp.model.bean.comptabilite;

import org.apache.commons.lang3.ObjectUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.*;
import static junit.framework.TestCase.assertEquals;


@RunWith(MockitoJUnitRunner.class)
public class EcritureComptableTest {

    @InjectMocks
    EcritureComptable ecritureComptable;

    List<CompteComptable> compteComptableList = new ArrayList<>();
    List<JournalComptable >journalComptableList =new ArrayList<>();


    @Before
    public void setupListesCompteJournal(){

        compteComptableList.add(new CompteComptable(401,"Fournisseurs" ) );
        compteComptableList.add(new CompteComptable(411,"Clients" ) );
        compteComptableList.add(new CompteComptable(4456,"Taxes sur le chiffre d'affaires déductibles" ) );
        compteComptableList.add(new CompteComptable(4457,"Taxes sur le chiffre d'affaires collectées par l'entreprise" ) );
        compteComptableList.add(new CompteComptable(512,"Banque" ) );
        compteComptableList.add(new CompteComptable(606,"Achats non stockés de matières et fournitures" ) );
        compteComptableList.add(new CompteComptable(706,"Prestations de services" ) );

        journalComptableList.add(new JournalComptable("AC","Achat") );
        journalComptableList.add(new JournalComptable("VE","Vente") );
        journalComptableList.add(new JournalComptable("BQ","Banque") );
        journalComptableList.add(new JournalComptable("OD","Opérations Diverses") );

    }

    /**
     * test de isEquilibree sur EcritureComptable
     */
    @Test
    public void isEquilibreeTest() {

        //Given
        ecritureComptable.setLibelle("Equilibrée");
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(1,-1,"Ecriture 11",411,"Clients","200.50", null));
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(2,-1,"Ecriture 12",401, "Fournisseurs","100.50", "33"));
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(1,-1,"Ecriture 21",401,"Fournisseurs",null, "301"));
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(2,-3,"Ecriture 22",512,"Banque","40", "7"));
        // When
        //Then
        assertTrue(ecritureComptable.toString(), ecritureComptable.isEquilibree());

        //Given
        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Non équilibrée");
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(1,-1,"Ecriture 1", 411,"Clients","200.50", null));
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(2, -1,"Ecriture 2",401, "Fournisseurs","100.50", "33"));
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(3, -1,"Ecriture 3",401,"Fournisseurs",null, "301"));
        // When
        //Then
        assertFalse(ecritureComptable.toString(), ecritureComptable.isEquilibree());

    }

    /**
     * test de isCumulDebitEstAuFormatDeuxDecimal - deux digit après la virgule
     */
    @Test
    public void isCumulDebitEstAuFormatDeuxDecimalTest() {

        ecritureComptable.setLibelle("Débit 12.50");
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(1,-1,"Ecriture 11",411,"Clients","8", null));
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(2,-1,"Ecriture 12",401, "Fournisseurs","4.50", "33"));

        assertEquals(ecritureComptable.toString(), ecritureComptable.getTotalDebit(), new BigDecimal("12.50") );

    }

    /**
     * test de isCumulCreditEstAuFormatDeuxDecimal - deux digit après la virgule
     */
    @Test
    public void isCumulCreditEstAuFormatDeuxDecimalTest() {

        ecritureComptable = new EcritureComptable();

        ecritureComptable.setLibelle("Crédit 12.50");
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(1,-1,"Ecriture 11",411,"Clients",null, "8.45"));
        ecritureComptable.getListLigneEcriture().add(buildLigneEcritureComptable(2,-1,"Ecriture 12",401, "Fournisseurs",null, "4.05"));

        assertEquals(ecritureComptable.toString(), ecritureComptable.getTotalCredit(), new BigDecimal("12.50") );
        ecritureComptable=null;
    }


    /**
     * Verifier si la référence est bien construite (utilsation de la regaex)
     */
    @Test
    public void isFormatReferenceConformeRegex() {

        ecritureComptable = new EcritureComptable();
        ecritureComptable.setReference("BQ-2021/00008");
        assertTrue("Erreur: le format n'est pas conforme à la regex", ecritureComptable.getReference().matches("[A-Z]{1,5}-\\d{4}/\\d{5}"));

        ecritureComptable=null;
    }

    /**
     * Verifier si le la réference correspond a un article du journal
     */
    @Test
    public void isReferenceCodeEqualJournalCode() {

        ecritureComptable = new EcritureComptable();
        ecritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
        ecritureComptable.setReference("BQ-2016/00003");
        assertEquals(ecritureComptable.getReference().substring(0, 2), ecritureComptable.getJournal().getCode());

        ecritureComptable=null;
    }

    /**
     * renseigne une ligneEcritureComptable
     * @param  pLigneId  Integer identifiant de la ligne
     * @param pLigneEcritureLibelle  String libelle de la ligne
     * @param pCompteComptableNumero  Integer id du CompteComptable
     * @param pCompteComptableLibelle String libelle comptecomptable
     * @param pDebit String Debit
     * @param pCredit String Credit
     * @return  LigneEcritureComptable
     */
    private LigneEcritureComptable buildLigneEcritureComptable(Integer pLigneId, Integer pEcrComptId , String pLigneEcritureLibelle, Integer pCompteComptableNumero, String pCompteComptableLibelle, String pDebit, String pCredit) {

        CompteComptable pCompteComptable =  ObjectUtils.defaultIfNull(
                CompteComptable.getByNumero( compteComptableList, pCompteComptableNumero ),
                new CompteComptable( pCompteComptableNumero,pCompteComptableLibelle ) );

        BigDecimal vDebit = pDebit == null ?  null : new BigDecimal( pDebit );
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal( pCredit );


        LigneEcritureComptable vRetour = new LigneEcritureComptable(pLigneId, pEcrComptId,pCompteComptable, pLigneEcritureLibelle,vDebit,vCredit );
        return vRetour;
    }


}
