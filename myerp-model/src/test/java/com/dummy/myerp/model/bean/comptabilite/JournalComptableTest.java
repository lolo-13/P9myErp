package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertNotEquals;


public class JournalComptableTest {

    @Test
    public void isLibelleJournalComptableCorrespondCodeJournalComptableTest() {
        List<JournalComptable> journalComptables = new ArrayList<>();

        journalComptables.add(new JournalComptable("AC", "Achat"));

        journalComptables.add(new JournalComptable("VE", "Vente"));


        assertEquals(journalComptables.get(0).toString(),
                JournalComptable.getByCode(journalComptables, "AC").getLibelle(), "Achat");

        assertNotEquals(JournalComptable.getByCode(journalComptables, "VE").getLibelle(), "Banque");

        assertNull(JournalComptable.getByCode(journalComptables, "ZZ"));
    }

    /**
     * verifier exxitence code journal
     */
    @Test
    public void isCodeJournalComptableExistTest() {
        JournalComptable journalComptable = new JournalComptable( "AC","Achat") ;

        assertEquals("AC", journalComptable.getCode());

        assertNotEquals("VI", journalComptable.getCode());

    }
}
