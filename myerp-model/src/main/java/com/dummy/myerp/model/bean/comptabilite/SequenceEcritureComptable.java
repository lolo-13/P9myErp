package com.dummy.myerp.model.bean.comptabilite;


import java.util.List;
import java.util.Objects;

/**
 * Bean représentant une séquence pour les références d'écriture comptable
 */
public class SequenceEcritureComptable {

    // ==================== Attributs ====================
    /** Le code journal */
    private String journalCode;
    /**
     * L'année
     */
    private Integer annee;
    /**
     * La dernière valeur utilisée
     */
    private Integer derniereValeur;

    // ==================== Constructeurs ====================

    /**
     * Constructeur
     */
    public SequenceEcritureComptable() {
    }

    /**
     * Constructeur
     *
     * @param pAnnee          -
     * @param pDerniereValeur -
     */
    public SequenceEcritureComptable(String pJournalCode, Integer pAnnee, Integer pDerniereValeur) {
        journalCode = pJournalCode;
        annee = pAnnee;
        derniereValeur = pDerniereValeur;
    }


    // ==================== Getters/Setters ====================
    public String getJournalCode() {
        return journalCode;
    }
    public void setJournalCode(String journalCode) {
        this.journalCode = journalCode;
    }
    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer pAnnee) {
        annee = pAnnee;
    }

    public Integer getDerniereValeur() {
        return derniereValeur;
    }

    public void setDerniereValeur(Integer pDerniereValeur) {
        derniereValeur = pDerniereValeur;
    }


    // ==================== Méthodes ====================
    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
                .append("annee=").append(annee)
                .append(vSEP).append("derniereValeur=").append(derniereValeur)
                .append("}");
        return vStB.toString();
    }


    public static SequenceEcritureComptable getByJournalCodeAndYear(List<? extends SequenceEcritureComptable> pList, String pCode, Integer pYear) {
        SequenceEcritureComptable vRetour = null;
        for (SequenceEcritureComptable vBean : pList) {
            if (isSequenceEcritureComptableExiste( vBean,  pCode, pYear )) {
                vRetour = vBean;
                break;
            }
        }
        return vRetour;
    }

    public static boolean isSequenceEcritureComptableExiste(SequenceEcritureComptable vBean, String pCode, Integer pYear){
        return (vBean != null && Objects.equals(vBean.getJournalCode(), pCode) && Objects.equals(vBean.getAnnee(), pYear));
    }
}
