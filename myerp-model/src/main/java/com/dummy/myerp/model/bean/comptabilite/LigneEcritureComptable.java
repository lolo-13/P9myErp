package com.dummy.myerp.model.bean.comptabilite;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.dummy.myerp.model.validation.constraint.MontantComptable;


/**
 * Bean représentant une Ligne d'écriture comptable.
 */
public class LigneEcritureComptable {


    @NotNull
    private Integer ligneId;

    private Integer ecritureComptableId;

    // ==================== Attributs ====================
    /** Compte Comptable */
    @NotNull
    private CompteComptable compteComptable;

    /** The Libelle. */
    @Size(max = 200)
    private String libelle;

    /** The Debit. */
    @MontantComptable
    private BigDecimal debit;

    /** The Credit. */
    @MontantComptable
    private BigDecimal credit;


    // ==================== Constructeurs ====================
    /**
     * Instantiates a new Ligne ecriture comptable.
     */
    public LigneEcritureComptable() {
    }

    /**
     * Instantiates a new Ligne ecriture comptable.
     *
     * @param pCompteComptable the Compte Comptable
     * @param pDebit the debit
     * @param pCredit the credit
     */
    public LigneEcritureComptable(int pligneId, int pecritureComptableId,
                                  CompteComptable pCompteComptable,
                                  String pLigneEcritureLibelle,
                                  BigDecimal pDebit, BigDecimal pCredit) {
        ligneId = pligneId;
        ecritureComptableId = pecritureComptableId;
        compteComptable = pCompteComptable;
        libelle = pLigneEcritureLibelle;
        debit = pDebit;
        credit = pCredit;
    }

    public LigneEcritureComptable(CompteComptable pCompteComptable,
                                  String pLigneEcritureLibelle,
                                  BigDecimal pDebit, BigDecimal pCredit) {
        compteComptable = pCompteComptable;
        libelle = pLigneEcritureLibelle;
        debit = pDebit;
        credit = pCredit;
    }

    public LigneEcritureComptable(Integer pLigneId, CompteComptable pCompteComptable,
                                  String pLigneEcritureLibelle, BigDecimal vDebit,
                                  BigDecimal vCredit) {
        ligneId = pLigneId;

        compteComptable = pCompteComptable;
        libelle = pLigneEcritureLibelle;
        debit = vDebit;
        credit = vCredit;
    }


    // ==================== Getters/Setters ====================
    public CompteComptable getCompteComptable() {
        return compteComptable;
    }
    public void setCompteComptable(CompteComptable pCompteComptable) {
        compteComptable = pCompteComptable;
    }
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String pLibelle) {
        libelle = pLibelle;
    }
    public BigDecimal getDebit() {
        return debit;
    }
    public void setDebit(BigDecimal pDebit) {
        debit = pDebit;
    }
    public BigDecimal getCredit() {
        return credit;
    }
    public void setCredit(BigDecimal pCredit) {
        credit = pCredit;
    }

    public Integer getLigneId() {
        return ligneId;
    }

    public void setLigneId(Integer ligneId) {
        this.ligneId = ligneId;
    }

    public Integer getEcritureComptableId() {
        return ecritureComptableId;
    }

    public void setEcritureComptableId(Integer ecritureComptableId) {
        this.ecritureComptableId = ecritureComptableId;
    }

    // ==================== Méthodes ====================
    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
                .append("compteComptable=").append(compteComptable)
                .append(vSEP).append("libelle='").append(libelle).append('\'')
                .append(vSEP).append("debit=").append(debit)
                .append(vSEP).append("credit=").append(credit)
                .append("}");
        return vStB.toString();
    }

    /*
     * Renvoie la liste des  LigneEcritureComptable dont l'ecritureComptableId == pid, si existe
     *
     */
    public static List<? extends LigneEcritureComptable> getLigneByEcritureComptableId(List<? extends LigneEcritureComptable> pLigneEcritureComptables , int pEcritureComptableId) {
        List<LigneEcritureComptable> ligneEcritureComptables = new ArrayList<>();
        for (LigneEcritureComptable x : pLigneEcritureComptables) {
            if (x.getEcritureComptableId() == pEcritureComptableId) {
                ligneEcritureComptables.add(x) ;
                break;
            }
        }
        return ligneEcritureComptables;
    }

    /*
     * Renvoie la LigneEcritureComptable dont le LigneId == pid, si  si elle est présente dans la liste.
     *
     */
    public static LigneEcritureComptable getLigneByIds(
            List<? extends LigneEcritureComptable> ligneEcritureComptables,
            int pLigneId,
            int pEcritureComptableId) {
        LigneEcritureComptable ligneEcritureComptable = null;
        for (LigneEcritureComptable x : ligneEcritureComptables) {
            if ((x.getEcritureComptableId() == pEcritureComptableId) &&
                    (x.getLigneId() == pLigneId)) {
                ligneEcritureComptable = x;
                break;
            }
        }
        return ligneEcritureComptable;
    }
}
