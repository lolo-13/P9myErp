package com.dummy.myerp.business.impl.manager;



import com.dummy.myerp.business.SpringRegistry;
import com.dummy.myerp.business.impl.TransactionManager;
import com.dummy.myerp.consumer.dao.impl.db.dao.ComptabiliteDaoImpl;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import com.dummy.myerp.business.BusinessTestCase;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.verification.Times;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.transaction.TransactionStatus;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ComptabiliteManagerIT extends BusinessTestCase {
    public ComptabiliteManagerIT() {
        super();
    }

    /*
    Spy sur les 3 classes principales pour permettre l'appel à Verify()
     */
    @Spy
    static ComptabiliteDaoImpl comptabiliteDao = ComptabiliteDaoImpl.getInstance();

    @Spy
    ComptabiliteManagerImpl comptabiliteManager = new ComptabiliteManagerImpl();

    @Spy
    TransactionManager transactionManager = TransactionManager.getInstance();

    /**
     *
     * @throws RuntimeException  : levée sur erreur regex dans le xml de config database
     */
    @Before
    public void setupDesTests() throws RuntimeException {
        assertNotNull(SpringRegistry.getBusinessProxy());
        assertNotNull(SpringRegistry.getTransactionManager());
        comptabiliteDao.resetDataBase();
    }


    /**
     * Test obtention de la liste des CompteComptables
     */
    @Test
    public void getListCompteComptableTest() {
        List<CompteComptable> listComptes = comptabiliteManager.getListCompteComptable();
        verify(comptabiliteManager, new Times(1)).getListCompteComptable();
        assertEquals(7, listComptes.size());
    }

    /**
     * Test obtention de la liste des JournalComptable
     */
    @Test
    public void getListJournalComptableTest() {
        List<JournalComptable> listJournaux = comptabiliteManager.getListJournalComptable();
        verify(comptabiliteManager, new Times(1)).getListJournalComptable();
        assertEquals( 4, listJournaux.size());
    }

    /**
     * Test obtention de la liste des EcritureComptables
     */
    @Test
    public void getListEcritureComptableTest() {
        List<EcritureComptable> listEcritures = comptabiliteManager.getListEcritureComptable();
        verify(comptabiliteManager, new Times(1)).getListEcritureComptable();
        assertEquals( 5, listEcritures.size());
    }

    /**
     * Test de la méthode addReference - fait partie des tests demandés par cahier des charges
     */
    @Test
    public void addReferenceTest() {
        //Given
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();

        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Cartouches d’imprimante");
        ecritureComptable.setReference("AC-2021/00001");


        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401,"Fournisseurs" ), "Facture c1",new BigDecimal("123.02"),null));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401,"Fournisseurs" ), "Facture c1",null,new BigDecimal("123.02")));

        //When
        comptabiliteManager.addReference(ecritureComptable);

        //Then
        verify(comptabiliteManager, new Times(1)).addReference(ecritureComptable);
        Assert.assertEquals(ecritureComptable.toString(), "AC-2021/00001", ecritureComptable.getReference());
    }

    /**
     *
     */
    @Test
    public void transactionManagerStatus(){
        TransactionStatus transactionStatus = null;
        try {
            transactionManager.commitMyERP(transactionStatus);
            Assert.assertNull( transactionStatus );
        } finally {
            transactionStatus = transactionManager.beginTransactionMyERP();
            Assert.assertNotNull( transactionStatus );
            transactionManager.rollbackMyERP(transactionStatus);
        }

        transactionStatus = transactionManager.beginTransactionMyERP();
        try {
            transactionManager.commitMyERP(transactionStatus);
            Assert.assertNotNull( transactionStatus );
            transactionStatus = null;
        } finally {
            Assert.assertNull( transactionStatus );
            transactionManager.rollbackMyERP(transactionStatus);
        }

    }


    /**
     * Test Insertion EcritureComptable
     *
     * @throws FunctionalException levée sur erreur de contraintes ou "SQL renvoie not found"
     * @throws NotFoundException  leveé si la requete SQL retourne "not found"
     */
    @Test
    public  void insertEcritureComptableTest() throws FunctionalException, NotFoundException {

        //Given
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();

        Calendar calendar = Calendar.getInstance();

        ecritureComptable.setDate(new Date());
        ecritureComptable.setJournal( new JournalComptable("AC","Achat") );

        ecritureComptable.setLibelle("Insertion nouvelle écriture comptable");

        ecritureComptable.getListLigneEcriture().add(this.buildLigneEcritureComptable(1,"Facture F1",411,"Nouveau Compte A", "102.01", null));
        ecritureComptable.getListLigneEcriture().add(this.buildLigneEcritureComptable(2,"Facture F2",401,"Nouveau Compte B",null, "102.01"));

        //When
        comptabiliteManager.addReference(ecritureComptable);

        //Then
        verify(comptabiliteManager, new Times(1)).addReference(ecritureComptable);
        assertEquals(ecritureComptable.toString(), "AC-2021/00001", ecritureComptable.getReference());

        //When
        comptabiliteManager.insertEcritureComptable( ecritureComptable );
        List<EcritureComptable> listEcritures = comptabiliteManager.getListEcritureComptable();

        //Then
        verify(comptabiliteManager, new Times(1)).insertEcritureComptable( ecritureComptable);
        assertEquals( 6, listEcritures.size());

        //When
        EcritureComptable ecritureComptableByRef = comptabiliteDao.getEcritureComptableByRef(ecritureComptable.getReference());
        comptabiliteManager.deleteEcritureComptable( ecritureComptableByRef.getId() );
        SequenceEcritureComptable pSeq = comptabiliteDao.getSequenceEcritureComptable("AC",calendar.get(Calendar.YEAR) );
        comptabiliteDao.deleteSequenceEcritureComptable( pSeq );

        //Then
        listEcritures = comptabiliteManager.getListEcritureComptable();
        assertEquals( 5, listEcritures.size());

    }

    /**
     *  test updateEcritureComptable Ecriture Comptable
     * @throws FunctionalException SQL renvoie vide ou erreur de vérificaiton de constraintes
     * @throws NotFoundException SQL renvoie vide
     */
    @Test
    public void updateEcritureComptable() throws FunctionalException, NotFoundException {
        //Given
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();
        Calendar calendar = Calendar.getInstance();
        ecritureComptable.setJournal( new JournalComptable("AC", "Achat") );
        ecritureComptable.setDate( new Date() );
        ecritureComptable.setLibelle("Insertion écriture");
        ecritureComptable.getListLigneEcriture().add(this.buildLigneEcritureComptable(1,"Facture F1",411,"Compte A", "102.23", null));
        ecritureComptable.getListLigneEcriture().add(this.buildLigneEcritureComptable(2, "Facture F2",401,"Compte B",null, "102.23"));

        //When
        comptabiliteManager.addReference(ecritureComptable);

        //Then
        verify(comptabiliteManager, new Times(1)).addReference(ecritureComptable);
        assertEquals(ecritureComptable.toString(), "AC-2021/00001", ecritureComptable.getReference());

        //When
        comptabiliteManager.insertEcritureComptable(ecritureComptable);
        //Then
        verify(comptabiliteManager, new Times(1)).insertEcritureComptable(ecritureComptable);
        assertEquals( 6, comptabiliteManager.getListEcritureComptable().size());

        //Given
        ecritureComptable.setLibelle("Mise à Jour");
        //When
        comptabiliteManager.updateEcritureComptable(ecritureComptable);
        EcritureComptable ecritureComptableByRef = comptabiliteDao.getEcritureComptableByRef(ecritureComptable.getReference());
        //Then
        verify(comptabiliteManager, new Times(1)).updateEcritureComptable(ecritureComptable);
        assertEquals( "Mise à Jour", ecritureComptableByRef.getLibelle());

        //Given

        // Wen
        // Suppression Ecriture et Sequence ( ne doit pas levée d'excpetion
        comptabiliteManager.deleteEcritureComptable(ecritureComptableByRef.getId());

        //Then
        verify(comptabiliteManager, new Times(1)).deleteEcritureComptable(ecritureComptableByRef.getId());
        assertEquals( 5, comptabiliteManager.getListEcritureComptable().size());

        SequenceEcritureComptable sequenceEcritureComptable = comptabiliteDao.getSequenceEcritureComptable("AC",calendar.get(Calendar.YEAR) );
        comptabiliteDao.deleteSequenceEcritureComptable(sequenceEcritureComptable);

    }

    /**
     * test delete EcritureComptable
     * @throws FunctionalException SQL renvoie vide ou erreur de vérificaiton de constraintes
     * @throws NotFoundException SQL renvoie vide
     */
    @Test
    public void deleteEcritureComptable() throws FunctionalException, NotFoundException {
        //Given
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();

        JournalComptable journalComptable = new JournalComptable("AC","Achat");
        CompteComptable compteComptable = new CompteComptable(512,"Banque");

        Calendar calendar = Calendar.getInstance();
        ecritureComptable.setJournal( journalComptable);
        ecritureComptable.setDate( new Date() );
        ecritureComptable.setLibelle("Insertion Ecriture");
        ecritureComptable.getListLigneEcriture().add(this.buildLigneEcritureComptable(1,"Facture F1",411,"Compte A", "102.23", null));
        ecritureComptable.getListLigneEcriture().add(this.buildLigneEcritureComptable(2, "Facture F2",401,"Compte B",null, "102.23"));

        //When  - Ajout de la référence AC-2021/00001
        comptabiliteManager.addReference(ecritureComptable);
        //Then
        verify(comptabiliteManager, new Times(1)).addReference(ecritureComptable);
        assertEquals(ecritureComptable.toString(), "AC-2021/00001", ecritureComptable.getReference());

        //When  - Insertion de cette nouvelle EcritureComptable
        comptabiliteManager.insertEcritureComptable(ecritureComptable);
        //Then
        verify(comptabiliteManager, new Times(1)).insertEcritureComptable(ecritureComptable);
        assertEquals( 6, comptabiliteManager.getListEcritureComptable().size());

        //Given
        EcritureComptable ecritureComptableByRef = comptabiliteDao.getEcritureComptableByRef(ecritureComptable.getReference());

        //When
        List<EcritureComptable> listEcritureComptable = comptabiliteManager.getListEcritureComptable();

        //Then  vérification que l'insert est OK
        assertNotNull(getEcritureComptableById( listEcritureComptable,ecritureComptableByRef.getId() ) );

        //When  Recupére  la liste des comptesComptables
        List<CompteComptable> listCompteComptable = comptabiliteManager.getListCompteComptable();
        //Then  vérifie que le comptecomptable "512" est bine là
        assertNotNull(CompteComptable.getByNumero(listCompteComptable,compteComptable.getNumero() ) );

        boolean isCodeJournal = false;
        //Given
        List<JournalComptable> listJournalComptable = comptabiliteManager.getListJournalComptable();
        //When
        for (JournalComptable jComptable : listJournalComptable ) {
            if( jComptable.getCode().equals( journalComptable.getCode() ) ) {
                isCodeJournal = true;
                break;
            }
        }
        //Then vérifie que le code Journal "AC" a bien été trouvé -> base est intègre
        assertTrue( isCodeJournal);

        //Given  Suppression de l'écriture
        comptabiliteManager.deleteEcritureComptable( ecritureComptable.getId() );
        //When
        listEcritureComptable = comptabiliteManager.getListEcritureComptable();
        //Then
        verify(comptabiliteManager, new Times(1)).deleteEcritureComptable( ecritureComptable.getId());
        assertNull( getEcritureComptableById( listEcritureComptable,ecritureComptableByRef.getId() ) );

        /*
         * Vérifie que rien d'autre n'est supprimé
         */
        //When
        listCompteComptable = comptabiliteManager.getListCompteComptable();
        //Then ok pour CompteComptable de code "512"
        assertNotNull(CompteComptable.getByNumero(listCompteComptable,compteComptable.getNumero() ) );

        isCodeJournal = false;
        //Given
        listJournalComptable = comptabiliteManager.getListJournalComptable();
        //When
        for (JournalComptable x : listJournalComptable ) {
            if( x.getCode().equals( journalComptable.getCode() ) ) {
                isCodeJournal = true;
                break;
            }
        }
        //Then  ok pour JournalComptable de code "AC"
        Assert.assertTrue( isCodeJournal );

        // Suppression SequenceEcriture - ne doit pas lever d'excpetion
        SequenceEcritureComptable sequenceEcritureComptable = comptabiliteDao.getSequenceEcritureComptable("AC",calendar.get(Calendar.YEAR) );
        comptabiliteDao.deleteSequenceEcritureComptable( sequenceEcritureComptable );
        verify(comptabiliteDao, new Times(1)).deleteSequenceEcritureComptable(sequenceEcritureComptable);
    }

    /**
     * Test  de l'insertion de SequenceEcritureComptable
     */
    @Test
    public void insertSequenceEcritureComptableTest() {
        //Given
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable("OD", 2021, 1);
        //When Then
        assertNull(comptabiliteDao.getSequenceEcritureComptable(sequenceEcritureComptable.getJournalCode(), sequenceEcritureComptable.getAnnee()));

        //Given
        comptabiliteManager.insertSequenceEcritureComptable(sequenceEcritureComptable);

        //When
        SequenceEcritureComptable sequence = comptabiliteDao.getSequenceEcritureComptable(sequenceEcritureComptable.getJournalCode(), sequenceEcritureComptable.getAnnee());
        //Then
        verify(comptabiliteManager, new Times(1)).insertSequenceEcritureComptable(sequenceEcritureComptable);
        assertEquals(sequenceEcritureComptable.getDerniereValeur(),sequence.getDerniereValeur());

        //Given
        comptabiliteDao.deleteSequenceEcritureComptable(sequence);
        //When Then
        assertNull(comptabiliteDao.getSequenceEcritureComptable(sequenceEcritureComptable.getJournalCode(), sequenceEcritureComptable.getAnnee()));
    }

    /**
     * Test  de la modification de de SequenceEcritureComptable
     */
    @Test
    public void updateSequenceEcritureComptableTest() {
        //Given
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable("OD", 2021, 1);
        comptabiliteManager.insertSequenceEcritureComptable(sequenceEcritureComptable);
        //When
        SequenceEcritureComptable sequence = comptabiliteDao.getSequenceEcritureComptable(sequenceEcritureComptable.getJournalCode(), sequenceEcritureComptable.getAnnee());
        //Then
        assertEquals(sequenceEcritureComptable.getDerniereValeur(),sequence.getDerniereValeur());

        //Given
        SequenceEcritureComptable sequenceEcritureComptableUpdate = new SequenceEcritureComptable("OD", 2021, 50);
        //When
        comptabiliteManager.updateSequenceEcritureComptable(sequenceEcritureComptableUpdate);
        sequence = comptabiliteDao.getSequenceEcritureComptable(sequenceEcritureComptableUpdate.getJournalCode(), sequenceEcritureComptableUpdate.getAnnee());
        //Then
        assertEquals(sequenceEcritureComptableUpdate.getDerniereValeur(),sequence.getDerniereValeur());

        //Given
        comptabiliteDao.deleteSequenceEcritureComptable(sequenceEcritureComptableUpdate);
        //When Then
        assertNull(comptabiliteDao.getSequenceEcritureComptable(sequenceEcritureComptable.getJournalCode(), sequenceEcritureComptable.getAnnee()));
    }

    /**
     * Vérification de la suppression d'une séquence d'écriture comptable.
     * Test passant
     */
    @Test
    public void deleteSequenceEcritureComptableTest()  {
        //Given
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable("OD", 2021, 1);
        comptabiliteManager.insertSequenceEcritureComptable(sequenceEcritureComptable);

        //When
        comptabiliteManager.deleteSequenceEcritureComptable(sequenceEcritureComptable);

        //Then
        assertNull(comptabiliteDao.getSequenceEcritureComptable(sequenceEcritureComptable.getJournalCode(), sequenceEcritureComptable.getAnnee()));
    }


    /**
     *  renseigne une ligneEcritureComptable
     * @param  pLigneId  Integer identifiant de la ligne
     * @param pLigneEcritureLibelle  String libelle de la ligne
     * @param pCompteComptableNumero  Integer id du CompteComptable
     * @param pCompteComptableLibelle String libelle comptecomptable
     * @param pDebit String Debit
     * @param pCredit String Credit
     * @return  LigneEcritureComptable
     */
    private LigneEcritureComptable buildLigneEcritureComptable(Integer pLigneId,
                                                               String pLigneEcritureLibelle,
                                                               Integer pCompteComptableNumero,
                                                               String pCompteComptableLibelle,
                                                               String pDebit, String pCredit) {
        List<CompteComptable> compteComptableList = new ArrayList<>();

        CompteComptable pCompteComptable =  ObjectUtils.defaultIfNull(
                CompteComptable.getByNumero( compteComptableList, pCompteComptableNumero ),
                new CompteComptable( pCompteComptableNumero,pCompteComptableLibelle ) );

        BigDecimal vDebit = pDebit == null ?  null : new BigDecimal( pDebit );
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal( pCredit );


        return new LigneEcritureComptable(pLigneId, pCompteComptable, pLigneEcritureLibelle,vDebit,vCredit );
    }

    /**
     * retrouve une ecriture par son Id
     *
     * @param  ecritureComptables List<? extends EcritureComptable> liste des écritures comptables
     * @param ecritureComptabeId Integer - id de ecriture comptable
     * @return  EcritureComptable si existe (null sinon)
     */
    private EcritureComptable getEcritureComptableById(List<? extends EcritureComptable> ecritureComptables, Integer ecritureComptabeId) {
        EcritureComptable ecritureComptable = null;
        for (EcritureComptable comptable : ecritureComptables) {
            if (isEcritureComptableIdEqualIdWeAreLookingFor(comptable,  ecritureComptabeId)) {
                ecritureComptable = comptable;
                break;
            }
        }
        return ecritureComptable;
    }

    /**
     *
     * @param ecritureComptable une EcritureComptable
     * @param ecritureComptabeId  id de l'écritureComptable à rechercher
     * @return true si l'id de l'écriture passée en paramètre == à l'id passé en paramètre
     */
    private boolean isEcritureComptableIdEqualIdWeAreLookingFor(EcritureComptable ecritureComptable, Integer ecritureComptabeId ){
        return (ecritureComptable != null && Objects.equals(ecritureComptable.getId(), ecritureComptabeId));
    }
}
