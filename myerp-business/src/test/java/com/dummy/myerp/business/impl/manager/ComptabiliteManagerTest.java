package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.business.impl.AbstractBusinessManager;
import com.dummy.myerp.business.impl.TransactionManager;
import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

import static com.dummy.myerp.business.impl.AbstractBusinessManager.configure;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ComptabiliteManagerTest {

    private ComptabiliteManagerImpl comptabiliteManager = new ComptabiliteManagerImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /*
     ******************************************************************************************************
     * EcritureComptableUnit (RG 2,3,5,6
     * ****************************************************************************************************
     */

    /**
     * Verif rg2,3,5,6 : OK
     *
     * @throws FunctionalException
     */
    @Test
    public void checkEcritureComptableUnit() throws FunctionalException {
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();

        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));

        ecritureComptable.setReference("AC-2021/00001");

        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Cartouches d’imprimante");

        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal("123.02"), null));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", null, new BigDecimal("123.02")));

        comptabiliteManager.checkEcritureComptableUnit(ecritureComptable);

    }
    /*
     ******************************************************************************************************
     * tests des contrôles sur attribut (DAO)
     * ****************************************************************************************************
     */

    /**
     * Vérification des contraintes unitaires sur les attributs de l'écriture : HS
     *
     * @throws FunctionalException
     */
    @Test(expected = FunctionalException.class)
    public void checkEcritureComptableUnitViolation() throws FunctionalException {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        comptabiliteManager.isContraintesUnitairesValides(vEcritureComptable);
    }
    /*
     ******************************************************************************************************
     * RG 2
     * ****************************************************************************************************
     */

    /**
     * RG2 : HS debit != de credit
     *
     * @throws FunctionalException
     */
    @Test(expected = FunctionalException.class)
    public void checkEcritureComptableUnitRG2Violation() throws FunctionalException {
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setReference("AC-2021/00001");
        ecritureComptable.setLibelle("Cartouches d’imprimante");

        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", null, new BigDecimal(123)));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", null, new BigDecimal(1234)));
        comptabiliteManager.isRG_Compta_2Valide(ecritureComptable);
    }


    /**
     * RG2 OK
     *
     * @throws FunctionalException
     */
    @Test
    public void checkEcritureComptableUnitRG2() throws FunctionalException {
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setReference("AC-2021/00001");
        ecritureComptable.setLibelle("Cartouches d’imprimante");

        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", null, new BigDecimal(123)));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal(123), null));
        comptabiliteManager.isRG_Compta_2Valide(ecritureComptable);
    }
    /*
     ******************************************************************************************************
     * RG 3
     * ****************************************************************************************************
     */

    /**
     * RG3 tests Ok
     */
    @Test
    public void checkEcritureComptableRG3Complet() {
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setReference("AC-2021/00001");

        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Nombre écriture valide : au moins 1  ligne de débit et 1 ligne de crédit");
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", null, new BigDecimal(11111)));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal(22222), null));
        assertTrue(comptabiliteManager.isRG_Compta_3Valide(ecritureComptable));

        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Nombre écriture valide : au moins 2 lignes de débit/crédit");
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal(333333), new BigDecimal(11111)));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal(22222), new BigDecimal(44444)));
        assertTrue(comptabiliteManager.isRG_Compta_3Valide(ecritureComptable));

        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Nombre écriture valide :  1 ligne de débit et 1 ligne de débit/crédit ");
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(1, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal(333333), null));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal(22222), new BigDecimal(44444)));

        assertTrue(comptabiliteManager.isRG_Compta_3Valide(ecritureComptable));

        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Nombre écriture non valide : 1 seule ligne de débit");
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal(22222), null));
        assertFalse(comptabiliteManager.isRG_Compta_3Valide(ecritureComptable));

        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Nombre écriture non valide : 1 seule ligne de crédit");
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", null, new BigDecimal(44444)));
        assertFalse(comptabiliteManager.isRG_Compta_3Valide(ecritureComptable));

        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Nombre écriture non valide : 1 seule ligne de débit/crédit ");
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", new BigDecimal(22222), new BigDecimal(44444)));
        assertFalse(comptabiliteManager.isRG_Compta_3Valide(ecritureComptable));

        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Nombre écriture non valide : 2 lignes de crédit");
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", null, new BigDecimal(44444)));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(401, "Fournisseurs"),
                "Facture c1", null, new BigDecimal(44444)));
        assertFalse(comptabiliteManager.isRG_Compta_3Valide(ecritureComptable));

        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.setLibelle("Nombre écriture non valide : 2 lignes de debit");
        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(2, -1,
                        new CompteComptable(401, "Fournisseurs"),
                        "Facture c1", new BigDecimal(44444), null));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(2, -1,
                new CompteComptable(411, "Fournisseurs"),
                "Facture c1", new BigDecimal(44444), null));
        assertFalse(comptabiliteManager.isRG_Compta_3Valide(ecritureComptable));
    }
    /*
     ******************************************************************************************************
     *   Cas RG5 -- addReference
     * ****************************************************************************************************
     */

    /**
     * Test addReference : OK
     *
     * @throws FunctionalException
     */
    @Test
    public void addReferenceTest() throws FunctionalException {
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setId(1);
        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Libelle");
        ecritureComptable.setReference("AC-2021/00006");

        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(2, -1,
                        new CompteComptable(401, "Fournisseurs"),
                        "Libelle l1", new BigDecimal(456), null));

        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(2, -1,
                        new CompteComptable(411, "Fournisseurs"),
                        "Libelle l1", null, new BigDecimal(456)));


        SequenceEcritureComptable vSequence =
                new SequenceEcritureComptable("AC", 2021, 5);

        DaoProxy daoProxy = Mockito.mock(DaoProxy.class);
        ComptabiliteDao comptabiliteDao = Mockito.mock(ComptabiliteDao.class);
        TransactionManager transactionManager = Mockito.mock(TransactionManager.class);

        Mockito.when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);
        Mockito.when(comptabiliteDao.getSequenceEcritureComptable("AC", 2021)).thenReturn(vSequence);

        configure(null, daoProxy, transactionManager);
        comptabiliteManager.addReference(ecritureComptable);

        Assert.assertEquals(ecritureComptable.toString(), "AC-2021/00006",
                ecritureComptable.getReference());
    }

    /**
     * Test addReference : OK initialisation de la séquence
     */
    @Test
    public void addReferenceWithoutSequenceTest() {
        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setId(1);
        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Libelle");
        ecritureComptable.setReference("AC-2021/00001");

        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(2, -1,
                        new CompteComptable(401, "Fournisseurs"),
                        "Libelle l1", new BigDecimal(456), null));

        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(2, -1,
                        new CompteComptable(411, "Fournisseurs"),
                        "Libelle l1", null, new BigDecimal(456)));


        DaoProxy daoProxy = Mockito.mock(DaoProxy.class);
        ComptabiliteDao comptabiliteDao = Mockito.mock(ComptabiliteDao.class);
        TransactionManager transactionManager = Mockito.mock(TransactionManager.class);

        Mockito.when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);
        Mockito.when(comptabiliteDao.getSequenceEcritureComptable("AC", 2021)).thenReturn(null);

        configure(null, daoProxy, transactionManager);
        comptabiliteManager.addReference(ecritureComptable);
        Assert.assertEquals(ecritureComptable.toString(), "AC-2021/00001", ecritureComptable.getReference());
    }

    /**
     * Cas RG5  : isRG_Compta_5Valide
     */
    @Test
    public void isRG_Compta_5ValideTest() {
        Date date = new Date();
        EcritureComptable ecritureComptable = new EcritureComptable();
        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));

        ecritureComptable.setReference("AC-2021/00001");
        ecritureComptable.setDate(date);
        assertTrue(comptabiliteManager.isRG_Compta_5Valide(ecritureComptable));

        ecritureComptable.setReference("AL-2019/00001");
        assertFalse(comptabiliteManager.isRG_Compta_5Valide(ecritureComptable));

        ecritureComptable.setReference("AL-2016/00001");
        assertFalse(comptabiliteManager.isRG_Compta_5Valide(ecritureComptable));

        ecritureComptable.setReference("AC-2016/00001");
        assertFalse(comptabiliteManager.isRG_Compta_5Valide(ecritureComptable));

        ecritureComptable.setReference("AC-2016/0000");
        assertFalse(comptabiliteManager.isRG_Compta_5Valide(ecritureComptable));

        ecritureComptable.setReference("AC-2019/0000");
        assertFalse(comptabiliteManager.isRG_Compta_5Valide(ecritureComptable));
    }
    /*
     ******************************************************************************************************
     * RG6
     * ****************************************************************************************************
     */

    /**
     * RG 6 : OK
     *
     * @throws NotFoundException
     * @throws FunctionalException
     */
    @Test
    public void checkEcritureComptableContext() throws NotFoundException, FunctionalException {

        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setId(1);
        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setReference("AC-2021/00001");
        ecritureComptable.setLibelle("Suze");

        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(1, -1,
                        new CompteComptable(401, "Buvard"),
                        "QQ l1", null, new BigDecimal(456)));
        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(2, -1,
                        new CompteComptable(411, "Al cool"),
                        "GG l1", new BigDecimal(456), null));

        DaoProxy daoProxy = Mockito.mock(DaoProxy.class);
        ComptabiliteDao comptabiliteDao = Mockito.mock(ComptabiliteDao.class);

        Mockito.when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);
        Mockito.when(comptabiliteDao.getEcritureComptableByRef("AC-2021/00001")).thenReturn(ecritureComptable);

        AbstractBusinessManager.configure(null, daoProxy, null);
        comptabiliteManager.checkEcritureComptableContext(ecritureComptable);
    }

    /**
     * RG6 : ecriture existe déja : HS exception FunctionnalExcepyion est levée
     *
     * @throws NotFoundException
     * @throws FunctionalException
     */
    @Test(expected = FunctionalException.class)
    public void checkEcritureComptableContextDouble() throws NotFoundException, FunctionalException {

        EcritureComptable ecritureComptable;
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setId(1);
        ecritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Pastis");
        ecritureComptable.setReference("AC-2021/00001");
        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(1, -1,
                        new CompteComptable(401, "Fournisseurs"),
                        "C l1", null, new BigDecimal(456)));
        ecritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(2, -1,
                        new CompteComptable(411, "Fournisseurs"),
                        "F l1", new BigDecimal(456), null));


        DaoProxy daoProxy = Mockito.mock(DaoProxy.class);
        ComptabiliteDao comptabiliteDao = Mockito.mock(ComptabiliteDao.class);

        Mockito.when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);
        Mockito.when(comptabiliteDao.getEcritureComptableByRef("AC-2021/00002")).thenReturn(ecritureComptable);


        EcritureComptable ecritureComptable2;
        ecritureComptable2 = new EcritureComptable();
        ecritureComptable2.setId(2);
        ecritureComptable2.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptable2.setDate(new Date());
        ecritureComptable2.setLibelle("Rhum");
        ecritureComptable2.setReference("AC-2021/00002");

        ecritureComptable2.getListLigneEcriture().add(
                new LigneEcritureComptable(1, -1,
                        new CompteComptable(401, "Fournisseurs"),
                        "A l1", null, new BigDecimal(456)));
        ecritureComptable2.getListLigneEcriture().add(
                new LigneEcritureComptable(2, -1,
                        new CompteComptable(411, "Fournisseurs"),
                        "B l1", new BigDecimal(456), null));


        AbstractBusinessManager.configure(null, daoProxy, null);
        comptabiliteManager.checkEcritureComptableContext(ecritureComptable2);
    }

}
